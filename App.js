import React from 'react';
import { StyleSheet, Text, View, WebView, NetInfo, Image } from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      netInfo: 'offline'
    }
  }

  render() {
    NetInfo.isConnected.fetch().then(isConnected => {
      this.setState({
        netInfo: isConnected ? 'online' : 'offline'
      })
    });
    return (
      <View style={styles.container}>
        {this.state.netInfo == 'online' ?
          <View style={{
            flex: 1, marginTop: 35, width: "100%", height: "100%", backgroundColor: "#E06965"
          }}>
            <WebView
              source={{ uri: 'http://13.233.54.241:3000' }}
              bounces={false}
              style={{
                flex: 1,
                width: "100%",
                height: "100%",

              }}
            />
          </View>
          :
          <View style={{
            flex: 1, width: "100%", height: "100%", backgroundColor: "#E06965"
          }}>
            <Image source={require('./assets/netInfo.png')}
              style={{
                height: "100%",
                width: "100%",
                resizeMode: "contain"
              }} />

          </View>}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E06965',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
